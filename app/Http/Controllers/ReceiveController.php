<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 23:04
 */

namespace App\Http\Controllers;

use App\Jobs\PaymentsJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ReceiveController.
 *
 * @package App\Http\Controllers
 */
class ReceiveController extends Controller {

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function receive(Request $request): JsonResponse {
        $paymentsPackage = $request->request->all();
        dispatch(new PaymentsJob($paymentsPackage));

        return new JsonResponse('', 200);
    }
}
