<?php

namespace App\Jobs;

use App\Models\Transaction;
use App\Models\UserWallet;
use Illuminate\Support\Facades\DB;

/**
 * Class PaymentsJob.
 *
 * @package App\Jobs
 */
class PaymentsJob extends Job {

    /**
     * @var array
     */
    protected $payments;

    /**
     * PaymentsJob constructor.
     *
     * @param array $paymentsPackage
     */
    public function __construct(array $paymentsPackage) {
        $this->payments = $paymentsPackage;
    }

    /**
     * @return void
     */
    public function handle(): void {
        DB::transaction(function() {
            [$inserts, $usersIds] = [[], []];
            foreach ($this->payments as $payment) {
                $inserts[] = [
                    'id'      => $payment['id'],
                    'user_id' => $payment['order_number'],
                    'sum'     => $payment['sum'] + ($payment['sum'] * $payment['commission'] / 100)
                ];

                $usersIds[] = $payment['order_number'];
            }

            $userWalletsInserts = [];
            $usersWallets       = UserWallet::query()->whereIn('user_id', $usersIds)->get()->keyBy('user_id');
            unset($usersIds);
            foreach ($inserts as $insert) {
                if ($usersWallets->has($insert['user_id'])) {
                    /** @var UserWallet $userWallet */
                    $userWallet      = $usersWallets->get($insert['user_id']);
                    $userWallet->sum += $insert['sum'];
                } else {
                    !array_key_exists($insert['user_id'], $userWalletsInserts) ? $userWalletsInserts[$insert['user_id']] = [
                        'user_id' => $insert['user_id'],
                        'sum'     => $insert['sum'],
                    ] : $userWalletsInserts[$insert['user_id']]['sum'] += $insert['sum'];
                }
            }

            Transaction::query()->insert($inserts);
            UserWallet::query()->insert($userWalletsInserts);

            /** @var UserWallet $wallet */
            foreach ($usersWallets as $wallet) {
                $wallet->save();
            }
        });
    }
}
