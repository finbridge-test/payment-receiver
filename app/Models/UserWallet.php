<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 19.04.20
 * Time: 13:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class UserWallet.
 *
 * @property int $id
 * @property int $user_id
 * @property float $sum
 *
 * @property-read Collection $transactions
 *
 * @package App\Models
 */
class UserWallet extends Model {

    /**
     * @var string
     */
    protected $table = 'user_wallet';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function transactions(): HasMany {
        return $this->hasMany(Transaction::class, 'user_id', 'user_id');
    }
}
