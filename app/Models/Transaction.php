<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 19.04.20
 * Time: 13:04
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction.
 *
 * @property string $id
 * @property int $user_id
 * @property float $sum
 *
 * @package App\Models
 */
class Transaction extends Model {

    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'sum'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;
}
