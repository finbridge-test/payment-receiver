<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWalletTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_wallet', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('user_id', false, true)->nullable(false)->comment('Идентификатор клиента');
            $table->float('sum')->nullable(false)->comment('Сумма на счету');

            $table->unique('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user_wallet');
    }
}
